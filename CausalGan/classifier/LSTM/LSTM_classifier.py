#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 23:14:39 2019

@author: chenxiaoxu
"""

from keras.models import Model
from keras.layers import Input, Dense, LSTM, multiply, concatenate, Activation, Reshape
from keras.layers import Conv1D, BatchNormalization, GlobalAveragePooling1D, Permute, Dropout
from keras.regularizers import l2
from tensorflow import set_random_seed

#from utils.constants import MAX_NB_VARIABLES, NB_CLASSES_LIST, MAX_TIMESTEPS_LIST
from classifier.utils.keras_utils import train_LSTM_classifier, evaluate_LSTM_classifier, set_trainable
from classifier.utils.layer_utils import AttentionLSTM

import numpy as np

regularization_weight = 5e-4

def generate_model(MAX_NB_VARIABLES, MAX_TIMESTEPS, NB_CLASS = 4):
    ip = Input(shape=(MAX_NB_VARIABLES, MAX_TIMESTEPS))

    x = ip
    x = LSTM(8)(x)
    x = Dropout(0.8)(x)

#     y = Permute((2, 1))(ip)
#     y = Conv1D(128, 8, padding='same', kernel_initializer='he_uniform')(y)
#     y = BatchNormalization()(y)
#     y = Activation('relu')(y)
#     y = squeeze_excite_block(y)

#     y = Conv1D(256, 5, padding='same', kernel_initializer='he_uniform')(y)
#     y = BatchNormalization()(y)
#     y = Activation('relu')(y)
#     y = squeeze_excite_block(y)

#     y = Conv1D(128, 3, padding='same', kernel_initializer='he_uniform')(y)
#     y = BatchNormalization()(y)
#     y = Activation('relu')(y)

#     y = GlobalAveragePooling1D()(y)

#     x = concatenate([x, y])

    out = Dense(NB_CLASS, activation='softmax')(x)

    model = Model(ip, out)
    model.summary()

    return model

def squeeze_excite_block(input):

    filters = input._keras_shape[-1] # channel_axis = -1 for TF

    se = GlobalAveragePooling1D()(input)
    se = Reshape((1, filters))(se)
    se = Dense(filters // 16,  activation='relu', kernel_initializer='he_normal', use_bias=False)(se)
    se = Dense(filters, activation='sigmoid', kernel_initializer='he_normal', use_bias=False)(se)
    se = multiply([input, se])
    
    return se




