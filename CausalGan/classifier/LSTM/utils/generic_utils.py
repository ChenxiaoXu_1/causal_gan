import numpy as np
import pandas as pd
import os
import matplotlib as mpl
import matplotlib.pylab as plt

mpl.style.use('seaborn-paper')

#from utils.constants import TRAIN_FILES, TEST_FILES, MAX_NB_VARIABLES, NB_CLASSES_LIST


def load_dataset_at(fold_index=None, normalize_timeseries=False, verbose=True) -> (np.array, np.array):
  #  if verbose: print("Loading train / test dataset : ", TRAIN_FILES[index], TEST_FILES[index])

    savePath = 'classifier/train_data/'
#    absoulte_path = '/Users/chenxiaoxu/Dropbox/BNL/Research/Granger/GAN/Classifier/MLSTM-FCN/data/'
    x_train_path = savePath + 'X_train.npy'
    y_train_path = savePath + 'y_train.npy'
    x_valid_path = savePath + 'X_valid.npy'
    y_valid_path = savePath + 'y_valid.npy'  
    
    
    X_train = np.load(x_train_path)
    y_train = np.load(y_train_path)
    X_valid = np.load(x_valid_path)
    y_valid = np.load(y_valid_path)
    X_test = np.load(x_valid_path)
    y_test = np.load(y_valid_path)    


    is_timeseries = True

    # extract labels Y and normalize to [0 - (MAX - 1)] range
    nb_classes = len(np.unique(y_train))
    y_train = (y_train - y_train.min()) / (y_train.max() - y_train.min()) * (nb_classes - 1)

    if is_timeseries:
        # scale the values
        if normalize_timeseries:
            X_train_mean = X_train.mean()
            X_train_std = X_train.std()
            X_train = (X_train - X_train_mean) / (X_train_std + 1e-8)

 #   if verbose: 
 #       print("Finished processing train dataset..")

    # extract labels Y and normalize to [0 - (MAX - 1)] range
    nb_classes = len(np.unique(y_valid))
    y_valid = (y_valid - y_valid.min()) / (y_valid.max() - y_valid.min()) * (nb_classes - 1)

    if is_timeseries:
        # scale the values
        if normalize_timeseries:
            X_valid_mean = X_valid.mean()
            X_valid_std = X_valid.std()
            X_valid = (X_valid - X_valid_mean) / (X_valid_std + 1e-8)

#    if verbose: 
#        print("Finished processing train dataset..")

    # extract labels Y and normalize to [0 - (MAX - 1)] range
    nb_classes = len(np.unique(y_test))
    y_test = (y_test - y_test.min()) / (y_test.max() - y_test.min()) * (nb_classes - 1)

    if is_timeseries:
        # scale the values
        if normalize_timeseries:
            X_test = (X_test - X_train_mean) / (X_train_std + 1e-8)

#    if verbose:
#        print("Finished loading test dataset..")
#        print()
#        print("Number of train samples : ", X_train.shape[0], "Number of test samples : ", X_test.shape[0])
#        print("Number of classes : ", nb_classes)
#        print("Sequence length : ", X_train.shape[-1])
#X:(Instance, Num of Variables, Time Window)
#y: (Instance, )
    return X_train, y_train, X_valid, y_valid, X_test, y_test, is_timeseries


def calculate_dataset_metrics(X_train):
    max_nb_variables = X_train.shape[1]
    max_timesteps = X_train.shape[-1]

    return max_timesteps, max_nb_variables


def cutoff_choice(dataset_id, sequence_length):
#    print("Original sequence length was :", sequence_length, "New sequence Length will be : ", MAX_NB_VARIABLES[dataset_id])
    choice = input('Options : \n'
                   '`pre` - cut the sequence from the beginning\n'
                   '`post`- cut the sequence from the end\n'
                   '`anything else` - stop execution\n'
                   'To automate choice: add flag `cutoff` = choice as above\n'
                   'Choice = ')

    choice = str(choice).lower()
    return choice


def cutoff_sequence(X_train, X_test, choice, dataset_id, sequence_length):
    assert MAX_NB_VARIABLES[dataset_id] < sequence_length, "If sequence is to be cut, max sequence" \
                                                                   "length must be less than original sequence length."
    cutoff = sequence_length - MAX_NB_VARIABLES[dataset_id]
    if choice == 'pre':
        if X_train is not None:
            X_train = X_train[:, :, cutoff:]
        if X_test is not None:
            X_test = X_test[:, :, cutoff:]
    else:
        if X_train is not None:
            X_train = X_train[:, :, :-cutoff]
        if X_test is not None:
            X_test = X_test[:, :, :-cutoff]
#    print("New sequence length :", MAX_NB_VARIABLES[dataset_id])
    return X_train, X_test


if __name__ == "__main__":
    pass