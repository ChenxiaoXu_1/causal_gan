#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 30 11:36:56 2019

@author: chenxiaoxu
"""

#import utils
import numpy as np
import sklearn 
import keras 
import time 

from tensorflow import set_random_seed

import numpy as np
from sklearn.metrics import classification_report
from sklearn.preprocessing import label_binarize
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix

from sklearn.preprocessing import LabelEncoder



class Classifier_FCN:
    def __init__(self, output_directory, seed, input_shape, nb_classes = 4, verbose=True):
        self.output_directory = output_directory
        self.seed = seed
        
        self.model = self.build_model(input_shape, nb_classes)
        
        if (verbose==True):
            self.model.summary()
        

    def build_model(self, input_shape, nb_classes):
        input_layer = keras.layers.Input(input_shape)

        conv1 = keras.layers.Conv1D(filters=128, kernel_size=8, padding='same')(input_layer)
        conv1 = keras.layers.normalization.BatchNormalization()(conv1)
        conv1 = keras.layers.Activation(activation='relu')(conv1)

        conv2 = keras.layers.Conv1D(filters=256, kernel_size=5, padding='same')(conv1)
        conv2 = keras.layers.normalization.BatchNormalization()(conv2)
        conv2 = keras.layers.Activation('relu')(conv2)

        conv3 = keras.layers.Conv1D(128, kernel_size=3,padding='same')(conv2)
        conv3 = keras.layers.normalization.BatchNormalization()(conv3)
        conv3 = keras.layers.Activation('relu')(conv3)

        gap_layer = keras.layers.pooling.GlobalAveragePooling1D()(conv3)

        output_layer = keras.layers.Dense(nb_classes, activation='softmax')(gap_layer)

        model = keras.models.Model(inputs=input_layer, outputs=output_layer)

        model.compile(loss='categorical_crossentropy', optimizer = keras.optimizers.Adam(), metrics=['accuracy'])

        reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.5, patience=50, min_lr=0.0001)       
        file_path = self.output_directory + 's' + str(self.seed) + 'best_model.hdf5'
        model_checkpoint = keras.callbacks.ModelCheckpoint(filepath=file_path, monitor='loss', save_best_only=True)
        self.callbacks = [reduce_lr,model_checkpoint]

        return model 

    def fit(self, x_train, y_train, x_val, y_val,y_true): 
		# x_val and y_val are only used to monitor the test loss and NOT for training  
        batch_size = 16
        nb_epochs = 15

        mini_batch_size = int(min(x_train.shape[0]/10, batch_size))

        start_time = time.time() 

        hist = self.model.fit(x_train, y_train, batch_size=mini_batch_size, epochs = nb_epochs, verbose = 2, validation_data=(x_val,y_val), callbacks = self.callbacks)
		
        duration = time.time() - start_time
        model = keras.models.load_model(self.output_directory+ 's' + str(self.seed) + 'best_model.hdf5')
        y_pred_prob = model.predict(x_val)

		# convert the predicted from binary to integer 
        y_pred = np.argmax(y_pred_prob , axis=1)
 #       save_logs(self.output_directory, hist, y_pred, y_true, duration)
  
        keras.backend.clear_session()
        
        return y_val, y_pred


def transform_labels(y_train,y_test,y_val=None):
    """
    Transform label to min equal zero and continuous
    For example if we have [1,3,4] --->  [0,1,2]
    """

    # no validation split
    # init the encoder
    encoder = LabelEncoder()
    # concat train and test to fit
    y_train_test = np.concatenate((y_train,y_test),axis =0)
    # fit the encoder
    encoder.fit(y_train_test)
    # transform to min zero and continuous labels
    new_y_train_test = encoder.transform(y_train_test)
    # resplit the train and test
    new_y_train = new_y_train_test[0:len(y_train)]
    new_y_test = new_y_train_test[len(y_train):]
    return new_y_train, new_y_test


def fit_classifier(modelSave_dir, seed, datasets_dict): 
    
    #(instance, timewindow, variables)    ß
    x_train = datasets_dict['x_train']
    y_train = datasets_dict['y_train']
    x_test = datasets_dict['x_test']
    y_test = datasets_dict['y_test']

    x_train = x_train.transpose((0,2,1))
    x_test = x_test.transpose((0,2,1))


    # make the min to zero of labels
    y_train,y_test = transform_labels(y_train,y_test)

    # save orignal y because later we will use binary
    y_true = y_test.astype(np.int64) 
    # transform the labels from integers to one hot vectors
    enc = sklearn.preprocessing.OneHotEncoder()
    enc.fit(np.concatenate((y_train,y_test),axis =0).reshape(-1,1))
    y_train = enc.transform(y_train.reshape(-1,1)).toarray()
    y_test = enc.transform(y_test.reshape(-1,1)).toarray()

    input_shape = x_train.shape[1:]

    classifier = Classifier_FCN(modelSave_dir, seed = seed, input_shape = input_shape, nb_classes = 4, verbose = False)
    y_onehot, y_hat = classifier.fit(x_train, y_train, x_test, y_test, y_true)

    y = np.argmax(y_onehot, axis = 1)

    return y, y_hat
