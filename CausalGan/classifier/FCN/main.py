#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 30 11:36:56 2019

@author: chenxiaoxu
"""

#import utils
import numpy as np
import sklearn 

from tensorflow import set_random_seed
from FCN_classifier import fit_classifier, Classifier_FCN, transform_labels

import numpy as np
from sklearn.metrics import classification_report
from sklearn.preprocessing import label_binarize
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix

modelSave_dir = 'modelSave/'

seed = 1000
np.random.seed(seed)
#random.seed(seed)
set_random_seed(seed)
        
datasets_dict = {}

datasets_dict['x_train'] = np.load('train_data/X_train.npy')
datasets_dict['y_train'] = np.load('train_data/y_train.npy')
datasets_dict['x_test'] = np.load('train_data/X_valid.npy')
datasets_dict['y_test'] = np.load('train_data/y_valid.npy')

y, y_hat  = fit_classifier(modelSave_dir, seed, datasets_dict)
     
print(classification_report(y, y_hat, digits=4))
print("accuracy: %.4f" % accuracy_score(y, y_hat))
print("f1 macro: %.4f" % f1_score(y, y_hat, average='macro'))