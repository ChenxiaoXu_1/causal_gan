import argparse
import math
import time

import torch
import torch.nn as nn
from models import normal_learning_model_1203, generator_learning_model_1203, classifier_learning_model_from_gen_1203

from models import LSTM_classifier

import numpy as np
from models import utils_multidatasource, Optim
import scipy
import sklearn
from sklearn import metrics
import matplotlib.pyplot as plt
import scipy.io as sio
from scipy.sparse.linalg import svds
from sklearn.preprocessing import normalize
from numpy import linalg as LA
import pickle
from torch.autograd import Variable
from scipy.io import loadmat
import pdb
import torch.nn.functional as F
from sklearn.metrics import f1_score
from scipy import spatial
from termcolor import colored

from sklearn.metrics import classification_report
import pickle

from scipy.stats import sem

import warnings


def extract(v):
    return v.data.storage().tolist()

def train_lstm(loader, data, model, criterion, optim, batch_size, class_weight):
    model.train();
    total_loss = 0;
    n_samples = 0;
    for inputs in loader.get_batches(data, batch_size, True):        
        X, Y, Labels = inputs[0], inputs[1], inputs[2]
        #Y = Y[:,:,0:model.y_dim]
        model.zero_grad();
        output = model(X);        
        #predict_all = torch.cat((predict, predict),dim=0)
        batch_loss = criterion(output, Labels); 
        batch_loss = torch.sum(batch_loss * class_weight)

        batch_loss.backward()
        total_loss += batch_loss.data.item();
        optim.step();
        n_samples += (output.size(0) * output.size(1));
    return total_loss / n_samples


def evaluate_lstm(loader, data, model, criterion, optim, batch_size, class_weight):
    total_loss = 0;
    n_samples = 0;
    Label_truth = torch.zeros((0,data[2].shape[1])) 
    Label_predict = torch.zeros((0,model.num_class)) 
    for inputs in loader.get_batches(data, batch_size, True):        
        X, Y, Labels = inputs[0], inputs[1], inputs[2]
        #Y = Y[:,:,0:model.y_dim]
        output = model(X);        
        loss_all = criterion(output, Labels); 
        loss_all = torch.sum(loss_all * class_weight)
        total_loss += torch.sum(loss_all).data.item();
        n_samples += (output.size(0) * output.size(1));            
        
        Label_predict = torch.cat((Label_predict, output),dim=0)
        Label_truth = torch.cat((Label_truth, Labels),dim=0)
    
    Label_predict = Label_predict.detach().numpy()
    
    Label_truth = np.argmax(Label_truth, axis=1)
    Label_predict = np.argmax(Label_predict, axis=1)
    
    classification_report_tst = classification_report(Label_truth, Label_predict, output_dict=True)    
    return total_loss / n_samples, classification_report_tst


def train_normal(loader, data, model, criterion, optim, batch_size):
    model.train();
    total_loss = 0;
    n_samples = 0;
    for inputs in loader.get_batches(data, batch_size, True):        
        X, Y, Labels = inputs[0], inputs[1], inputs[2]
        #Y = Y[:,:,0:model.y_dim]
        model.zero_grad();
        output = model(X);        
        #predict_all = torch.cat((predict, predict),dim=0)
        batch_loss = criterion(output, Y); 

        batch_loss.backward()
        total_loss += batch_loss.data.item();
        optim.step();
        if len(output.shape)<4:
            n_samples += (output.size(0) * output.size(1) * output.size(2));
        else:
            n_samples += (output.size(0) * output.size(1) * output.size(2) * output.size(3));
    return total_loss / n_samples


def train_classifier(loader, data, model, criterion, optim, batch_size):
    model.train();
    total_loss = 0;
    n_samples = 0;
    #Label_truth = data[2].detach().numpy()
    Label_truth = torch.zeros((0,data[2].shape[1])) 
    Label_predict = torch.zeros((0,data[2].shape[1])) 
    for inputs in loader.get_batches(data, batch_size, True):        
        X, Y, Labels = inputs[0], inputs[1], inputs[2]
        #Y = Y[:,:,0:model.y_dim]
        model.zero_grad();
        output = model(X, model_normal);        
        #predict_all = torch.cat((predict, predict),dim=0)
        
        Y_duplicate = Y.clone()
        for class_idx in range(Labels.shape[1]-1):
            Y_duplicate = torch.cat((Y_duplicate, Y),dim=0)
        loss_all = criterion(output, Y_duplicate); 
        
        Label_weighting = Labels[:,0]
        for class_idx in range(Labels.shape[1]-1):
            if torch.sum(Labels[:,class_idx+1])>0:
                ratio = torch.sum(Labels[:,0])/torch.sum(Labels[:,class_idx+1])
            else:
                ratio = 1
            Label_weighting = torch.cat((Label_weighting, ratio*Labels[:,class_idx+1]),dim=0) 

        Label_weighting.unsqueeze_(-1)
        Label_weighting.unsqueeze_(-1)
        Label_weighting.unsqueeze_(-1)
        Label_weighting_new = Label_weighting.expand(len(Label_weighting),output.shape[1],output.shape[2],output.shape[3])   
            
        batch_loss = torch.sum(torch.mul(loss_all, Label_weighting_new))    
        batch_loss.backward()
        total_loss += batch_loss.data.item();
        optim.step();
        if len(output.shape)<4:
            n_samples += (output.size(0) * output.size(1) * output.size(2));
        else:
            n_samples += (output.size(0) * output.size(1) * output.size(2) * output.size(3));

        #################### classification evaluation ##############
        predict_label_tmp = torch.sum(loss_all, dim=1)
        predict_label_tmp = torch.sum(predict_label_tmp, dim=1)
        predict_label_tmp = torch.sum(predict_label_tmp, dim=1)
        predict_label = predict_label_tmp.view(Labels.shape[1], Y.shape[0]).transpose(0,1)
        predict_label = F.softmin(predict_label, dim =1)  
        Label_predict = torch.cat((Label_predict, predict_label),dim=0)
        Label_truth = torch.cat((Label_truth, Labels),dim=0)
    
    Label_predict = Label_predict.detach().numpy()
    classification_report_trn = classification_report(np.argmax(Label_truth, axis=1), np.argmax(Label_predict, axis=1), output_dict=True)    
    
    return total_loss / n_samples, classification_report_trn


def evaluate_classifier(loader, data, model, criterion, optim, batch_size):
    total_loss = 0;
    n_samples = 0;
    Label_truth = torch.zeros((0,data[2].shape[1])) 
    Label_predict = torch.zeros((0,data[2].shape[1])) 
    for inputs in loader.get_batches(data, batch_size, True):        
        X, Y, Labels = inputs[0], inputs[1], inputs[2]
        #Y = Y[:,:,0:model.y_dim]
        output = model(X, model_normal);        
        #predict_all = torch.cat((predict, predict),dim=0)
        output = output[0:Labels.shape[0]*Labels.shape[1]]
        Y_duplicate = Y.clone()
        for class_idx in range(Labels.shape[1]-1):
            Y_duplicate = torch.cat((Y_duplicate, Y),dim=0)
        loss_all = criterion(output, Y_duplicate); 
        
        Label_weighting = Labels[:,0]
        for class_idx in range(Labels.shape[1]-1):
            if torch.sum(Labels[:,class_idx+1])>0:
                ratio = torch.sum(Labels[:,0])/torch.sum(Labels[:,class_idx+1])
            else:
                ratio = 1
            Label_weighting = torch.cat((Label_weighting, ratio*Labels[:,class_idx+1]),dim=0) 

        #################### regression loss ##############
        Label_weighting.unsqueeze_(-1)
        Label_weighting.unsqueeze_(-1)
        Label_weighting.unsqueeze_(-1)
        Label_weighting_new = Label_weighting.expand(len(Label_weighting),output.shape[1],output.shape[2],output.shape[3])   
        batch_loss = torch.sum(torch.mul(loss_all, Label_weighting_new))    
        total_loss += batch_loss.data.item();
        if len(output.shape)<4:
            n_samples += (output.size(0) * output.size(1) * output.size(2));
        else:
            n_samples += (output.size(0) * output.size(1) * output.size(2) * output.size(3));
            
        
        #################### classification evaluation ##############
        predict_label_tmp = torch.sum(loss_all, dim=1)
        predict_label_tmp = torch.sum(predict_label_tmp, dim=1)
        predict_label_tmp = torch.sum(predict_label_tmp, dim=1)
        predict_label = predict_label_tmp.view(Labels.shape[1], Y.shape[0]).transpose(0,1)
        predict_label = F.softmin(predict_label, dim =1)  
        Label_predict = torch.cat((Label_predict, predict_label),dim=0)
        Label_truth = torch.cat((Label_truth, Labels),dim=0)
    
    Label_predict = Label_predict.detach().numpy()
    
    classification_report_tst = classification_report(np.argmax(Label_truth, axis=1), np.argmax(Label_predict, axis=1), output_dict=True)    
    return total_loss / n_samples, classification_report_tst


def evaluate_classifier2(loader, data, model, criterion, optim, batch_size):
    total_loss = 0;
    n_samples = 0;
    Label_truth = torch.zeros((0,data[2].shape[1])) 
    Label_predict = torch.zeros((0,model.num_class)) 
    for inputs in loader.get_batches(data, batch_size, True):        
        X, Y, Labels = inputs[0], inputs[1], inputs[2]
        #Y = Y[:,:,0:model.y_dim]
        output = model(X, model_normal);        
        #predict_all = torch.cat((predict, predict),dim=0)
        #output = output[0:Labels.shape[0]*Labels.shape[1]]
        Y_duplicate = Y.clone()
        for class_idx in range(model.num_class-1):
            Y_duplicate = torch.cat((Y_duplicate, Y),dim=0)
        loss_all = criterion(output, Y_duplicate); 
        total_loss += torch.sum(loss_all).data.item();
        if len(output.shape)<4:
            n_samples += (output.size(0) * output.size(1) * output.size(2));
        else:
            n_samples += (output.size(0) * output.size(1) * output.size(2) * output.size(3));
                    
        
        #################### classification evaluation ##############
        predict_label_tmp = torch.sum(loss_all, dim=1)
        predict_label_tmp = torch.sum(predict_label_tmp, dim=1)
        predict_label_tmp = torch.sum(predict_label_tmp, dim=1)
        predict_label = predict_label_tmp.view(model.num_class, X[0].shape[0]).transpose(0,1)
        predict_label = F.softmin(predict_label, dim =1)  
        Label_predict = torch.cat((Label_predict, predict_label),dim=0)
        Label_truth = torch.cat((Label_truth, Labels),dim=0)
    
    Label_predict = Label_predict.detach().numpy()
    
    Label_truth = np.argmax(Label_truth, axis=1)
    Label_predict = np.argmax(Label_predict, axis=1)
    
    for class_i in range(data[2].shape[1],model.num_class):
        Label_predict[Label_predict==class_i] = class_i-(data[2].shape[1])
        
    #Label_predict[Label_predict==3] = 11
    
    classification_report_tst = classification_report(Label_truth, Label_predict, output_dict=True)    
    return total_loss / n_samples, classification_report_tst

class args:
     

    train = 0.9 ## 
    valid = 0.05##

    
    
    model_normal = 'normal_learning_model_1203'
    model_generator = 'generator_learning_model_1203'
    model_classifier = 'classifier_learning_model_from_gen_1203'#'generator_learning_model'
    
    window = 18           ### classifier_win + classifier_prewin
    pre_win = 3 
    normal_win = 18
    normal_prewin = 3
    generator_win = 18    ### classifier_win + classifier_prewin
    generator_prewin = 1  ### 1 for focusing ####
    classifier_win = 15
    classifier_prewin = 3
    
    y_dim = 65   #### input dimensions

    RUC_layers = 1
    hidden_dim = 40
    reduce_dim = 30
    
    lowrank_normal = 60
    lowrank_minor = 1
    
        
    clip = 1.
    epochs_simple = 10
    lr_N = 0.02###############
    lr_G = 0.01
    lr_C = 0.01
    lr_lstm = 0.01
    
    batch_size = 100
    dropout = 0.001
    gpu = None
    cuda = False
    optim = 'adam'#'adam'

    weight_decay = 0
    horizon = 1
    output_fun = None
    mask = False

    
## initialize models
warnings.filterwarnings('ignore')
Data = utils_multidatasource.Data_utility(args)
###

#Linux:
#absolute_path = '/home/chenxiao/Dropbox/BNL/Research/Granger/supervise_learning/supervise_granger/syntheticData_modeltune_270919/A/'
#Macbook:
absolute_path = '/Users/chenxiaoxu/Dropbox/BNL/Research/Granger/supervise_learning/supervise_granger/syntheticData_modeltune_270919/A/'

data_paths = ['/10-20-30-edges/filter_norm_expression0.mat', '/10-20-30-edges/filter_norm_expression1.mat', 
              '/10-20-30-edges/filter_norm_expression2.mat', '/10-20-30-edges/filter_norm_expression3.mat']
graph_paths = ['/10-20-30-edges/A0.mat', '/10-20-30-edges/A1.mat', 
               '/10-20-30-edges/A2.mat', '/10-20-30-edges/A3.mat']
#data_paths = ['B_10022019/20-30-40-edges/filter_norm_expression0.mat', 'B_10022019/20-30-40-edges/filter_norm_expression1.mat', 
#              'B_10022019/20-30-40-edges/filter_norm_expression2.mat', 'B_10022019/20-30-40-edges/filter_norm_expression3.mat']
#graph_paths = ['B_10022019/20-30-40-edges/A0.mat', 'B_10022019/20-30-40-edges/A1.mat', 
#               'B_10022019/20-30-40-edges/A2.mat', 'B_10022019/20-30-40-edges/A3.mat']
traning_samples = [1000, 50, 50, 50]
testing_samples = [1000, 100, 100, 100]

data_all = []
G_groudtruth = []
for idx_data in range(len(data_paths)):
    data_tmp = sio.loadmat(absolute_path + data_paths[idx_data])['expression']
    graph_tmp = sio.loadmat(absolute_path + graph_paths[idx_data])['A']
    data_all.append(data_tmp)
    G_groudtruth.append(graph_tmp)

gen_num = 100
criterion_1 = nn.MSELoss(size_average=False)
criterion_2 = nn.MSELoss(size_average=False, reduce=False)
     

for start_point in [20,40,60]:
    
    for round_i in [0,1,2]:#,1,2,3,4]:
    
        Data.m = data_all[0].shape[1]
        print('buliding model')
        
        ## initializing models
        args.num_class = 1
        model_normal = eval(args.model_normal).Model(args, Data);
        optim_normal = Optim.Optim(
            model_normal.parameters(), args.optim, args.lr_N, args.clip, weight_decay = args.weight_decay,
        )
        
        
        ###############################
        ###### preparing dataset ######
        gap_point = 100
        
        X_trn_org = torch.zeros((0, args.window, Data.m)) ## input
        Y_trn_full_org = torch.zeros((0, args.window, args.pre_win, Data.m)) ### long target
        Label_trn_org = torch.zeros((np.sum(traning_samples), len(data_paths)))   ### label
        start_trn_idx = 0
        
        X_tst_org = torch.zeros((0, args.window, Data.m))
        Y_tst_full_org = torch.zeros((0, args.window, args.pre_win, Data.m))
        Label_tst_org = torch.zeros((np.sum(testing_samples), len(data_paths)))
        start_tst_idx = 0
        ### concatenating data class by class 
        for idx_data in range(len(data_paths)):
            
            data_tmp_train, data_tmp_valid, data_tmp_test = Data._split(data_all[idx_data], args)
            
            X_trn_tmp = data_tmp_train[0][start_point:(start_point+traning_samples[idx_data])]    
            X_trn_org = torch.cat((X_trn_org, X_trn_tmp), dim = 0)
            
            Y_trn_full_tmp = data_tmp_train[3][start_point:(start_point+traning_samples[idx_data])]  
            Y_trn_full_org = torch.cat((Y_trn_full_org, Y_trn_full_tmp), dim = 0)
            
            Label_trn_org[start_trn_idx:(start_trn_idx+traning_samples[idx_data]), idx_data] = 1   
            
            start_trn_idx = start_trn_idx + traning_samples[idx_data]        
            print(X_trn_org.shape)
            
            X_tst_tmp = data_tmp_train[0][(start_point+traning_samples[idx_data]+gap_point):(start_point+traning_samples[idx_data]+gap_point+testing_samples[idx_data])] 
            Y_tst_full_tmp = data_tmp_train[3][(start_point+traning_samples[idx_data]+gap_point):(start_point+traning_samples[idx_data]+gap_point+testing_samples[idx_data])]     
          
            X_tst_org = torch.cat((X_tst_org, X_tst_tmp), dim = 0)
            Y_tst_full_org = torch.cat((Y_tst_full_org, Y_tst_full_tmp), dim = 0)
            
            Label_tst_org[start_tst_idx:(start_tst_idx+testing_samples[idx_data]), idx_data] = 1   
            
            start_tst_idx = start_tst_idx + testing_samples[idx_data]   
            print(X_tst_org.shape)     
        
        ## copy of normal data (major data)
        trn_indices_normal = np.arange(traning_samples[0])
        np.random.shuffle(trn_indices_normal)
        X_trn_normal = X_trn_org[0:traning_samples[0]][trn_indices_normal] 
        Y_trn_full_normal = Y_trn_full_org[0:traning_samples[0]][trn_indices_normal]
        Data_trn_full_normal = [X_trn_normal, Y_trn_full_normal, Label_trn_org[0:traning_samples[0]][trn_indices_normal]]

        ## copy of event data (minor data)
        trn_indices_minor = np.arange(np.sum(traning_samples[1:]))
        np.random.shuffle(trn_indices_minor)
        X_trn_minor = X_trn_org[traning_samples[0]:][trn_indices_minor] 
        Y_trn_full_minor = Y_trn_full_org[traning_samples[0]:][trn_indices_minor]
        Data_trn_full_minor = [X_trn_minor, Y_trn_full_minor, Label_trn_org[traning_samples[0]:][trn_indices_minor]]
        
        #### prepraing training set (normal and event) for LSTM  ######
        Data_trn_full = [X_trn_org[:,0:args.classifier_win,:], Y_trn_full_org[:,0:args.classifier_win,:,:], Label_trn_org]
        #### preparing testing set #################
        Data_tst_full = [X_tst_org[:,0:args.classifier_win,:], Y_tst_full_org[:,0:args.classifier_win,:,:], Label_tst_org]


        print("~~~~~~~~~~~~~~~~~~~ begin training/validating ~~~~~~~~~~~~~~~~")          
        ##############################
        ####### normal training ######
        ##############################  
        G_groundtruth_normal = G_groudtruth[0].reshape(Data.m*Data.m); 
        for epoch in range(0, args.epochs_simple):
            epoch_start_time = time.time() 
            train_normal_loss = train_normal(Data, Data_trn_full_normal, model_normal, criterion_1, optim_normal, args.batch_size)
            G_predict, G_predict_org = model_normal.predict_relationship()   
            G_predict_normal = G_predict[0].reshape(Data.m*Data.m);
            precision, recall, thresholds1 = metrics.precision_recall_curve(G_groundtruth_normal, G_predict_normal)
            aupr = metrics.auc(recall, precision)
            print('Normal R{:3d}|epoch{:3d}|time:{:5.2f}s|tn_ls {:5.6f}|aupr {:5.6f}|'.format(round_i, epoch, (time.time() - epoch_start_time), train_normal_loss, aupr)) 

        ########################################################
        ########## initializing generator/classifier ###########
        ########################################################
        args.num_class = len(data_paths)    ###### generator of GAN ######   
        model_generator = eval(args.model_generator).Model(args, Data, model_normal);
        optim_generator = Optim.Optim(
            model_generator.parameters(), args.optim, args.lr_G, args.clip, weight_decay = args.weight_decay,
        )  
        
        args.num_class = len(data_paths) + len(data_paths)  ###### classifier of GAN ######  
        model_classifier = eval(args.model_classifier).Model(args, Data, model_normal);         
        optim_classifier = Optim.Optim(
            model_classifier.parameters(), args.optim, args.lr_C, args.clip, weight_decay = args.weight_decay,
        )
        

        ############################################
        ####### generator/classifier training ######
        ############################################
        classifier_trn_loss_old = 1000000
        tst_report_old = None
        for c_index in range(15):
            
            epoch_start_time = time.time() 


            ########## preparing input for generator ##########
 #           trn_indices_normal_gen = np.arange(traning_samples[0])
 #           np.random.shuffle(trn_indices_normal_gen)
 #           trn_indices_normal_gen = trn_indices_normal_gen[0:gen_num]
 #           X_trn_normal_gen       =     X_trn_org[0:traning_samples[0]][trn_indices_normal_gen] 
 #           Y_trn_full_normal_gen = Y_trn_full_org[0:traning_samples[0]][trn_indices_normal_gen]
 #           Data_trn_full_normal_gen = [X_trn_normal_gen, Y_trn_full_normal_gen, Label_trn_org[0:traning_samples[0]][trn_indices_normal_gen]]
            
            trn_noise_gen = torch.randn((gen_num, args.generator_win, Data.m))
            Data_trn_full_normal_gen = [trn_noise_gen, None, None]
                
            ################ generator generating fake data for classifier ######################         
            fake_data_all = model_generator(Data_trn_full_normal_gen, model_normal); 
            fake_data_X = fake_data_all[:, 0:args.classifier_win, :]  ### cut data
            fake_data_Y = torch.zeros((fake_data_all.shape[0], args.classifier_win, args.classifier_prewin, Data.m))
            for pre_win_i in range(args.classifier_prewin):
                fake_data_Y[:, :, pre_win_i, :] = fake_data_all[:, (pre_win_i+1):(pre_win_i+1+args.classifier_win), :]
            fake_data_label = torch.zeros((len(fake_data_X), len(data_paths)))
            for fake_class_i in range(len(data_paths)):
                fake_sampleidx_start = fake_class_i*gen_num
                fake_sampleidx_end   = (fake_class_i+1)*gen_num
                fake_data_label[fake_sampleidx_start:fake_sampleidx_end, fake_class_i] = 1
                    
                    
            ########### prepraing training input for classifier by merging real and fake data ############        
            X_trn_merged = torch.cat((X_trn_org[:, 0:args.classifier_win,:], fake_data_X), dim = 0)    #(1550, 15, 65)
            Y_trn_full_merged = torch.cat((Y_trn_full_org[:, 0:args.classifier_win, :, :], fake_data_Y), dim = 0)
            Label_merged = torch.zeros((len(X_trn_merged), 2*len(data_paths)))   ### label
            Label_merged[0:len(Label_trn_org), 0:len(data_paths)] = Label_trn_org  
            Label_merged[len(Label_trn_org):, len(data_paths):] = fake_data_label  
            Data_trn_merge = [X_trn_merged, Y_trn_full_merged, Label_merged]   
            ################## train and update classifier ###################
            classifier_trn_loss, trn_report = train_classifier(Data, Data_trn_merge, model_classifier, criterion_2, optim_classifier, args.batch_size)
            trn_macro_f1 = trn_report['macro avg']['f1-score']   

            
            ###################### update generator #############
            for g_index in range(2):
                
                model_generator.zero_grad()
                ################## get regression feedback for generator from classifier ##########
                prediction_from_classifier = model_classifier([fake_data_X], model_normal) #fake_data_X: 400, 15, 65
                
                ################## get label loss of generator output and update generator ##########
                fake_data_Y_duplicate = fake_data_Y.clone()
                for class_idx in range(model_classifier.num_class-1):
                    fake_data_Y_duplicate = torch.cat((fake_data_Y_duplicate, fake_data_Y),dim=0)
                fake_loss_all = criterion_2(prediction_from_classifier, fake_data_Y_duplicate); 
                feedback_label_tmp = torch.sum(fake_loss_all, dim=1)
                feedback_label_tmp = torch.sum(feedback_label_tmp, dim=1)
                feedback_label_tmp = torch.sum(feedback_label_tmp, dim=1)
                predict_label_soft = feedback_label_tmp.view(2*len(data_paths), fake_data_X.shape[0]).transpose(0,1)            
                predict_label_soft_invert = 1/(predict_label_soft)#torch.exp(1/(predict_label_soft+1))
                predict_label_soft_norm = F.normalize(predict_label_soft_invert, p=1, dim=1)           
                fake_labels_groundtruth = torch.zeros((predict_label_soft_norm.shape))
                for class_idx in range(model_generator.num_class):
                    start_idx_label = class_idx*gen_num
                    end_idx_label = (class_idx+1)*gen_num
                    fake_labels_groundtruth[start_idx_label:end_idx_label, class_idx] = 1
                generator_loss = criterion_1(predict_label_soft_norm, fake_labels_groundtruth);    ##### try different loss !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                generator_loss.backward(retain_graph=True)
                optim_generator.step()  
                generator_loss_sum = torch.sum(generator_loss)
            
            
            ##################### testing classifier ######################
            classifier_tst_loss, tst_report = evaluate_classifier2(Data, Data_tst_full, model_classifier, criterion_2, optim_classifier, args.batch_size)
            tst_macro_f1 = tst_report['macro avg']['f1-score']       
            G_predict_cls, G_predict_cls_org = model_classifier.predict_relationship(model_normal)  
            
            G_AUPR = np.zeros((len(G_groudtruth)))
            for graph_i in range(len(G_groudtruth)):
                G_groundtruth_i = G_groudtruth[graph_i].reshape(Data.m*Data.m); 
                G_predict_cls_i = G_predict_cls[graph_i].reshape(Data.m*Data.m);
                precision, recall, thresholds1 = metrics.precision_recall_curve(G_groundtruth_i, G_predict_cls_i)      
                G_AUPR[graph_i] = metrics.auc(recall, precision)    

            
            
            if classifier_trn_loss > classifier_trn_loss_old:
                print('break !! GAN Rround {:3d}|epoch {:3d}|time:{:5.2f}s|previous classifier trn loss {:5.4f} < current classifier trn loss {:5.4f}'.format(round_i, c_index, (time.time() - epoch_start_time), classifier_trn_loss_old, classifier_trn_loss)) 
                print(tst_report_old)
                break
            else:
                classifier_trn_loss_old = classifier_trn_loss
                tst_report_old = tst_report
                print('GAN Rround {:3d}|epoch {:3d}|T:{:5.2f}s|classifier trn loss {:5.4f}|generator loss {:5.4f}|testset MF1 {:5.4f}|'.format(round_i, c_index, (time.time() - epoch_start_time), classifier_trn_loss, generator_loss_sum, tst_macro_f1))
                print(G_AUPR)


             
                