import torch
import numpy as np;
from torch.autograd import Variable
import time
from scipy.io import loadmat
from sklearn.preprocessing import StandardScaler
import pickle

def normal_std(x):
    return x.std() * np.sqrt((len(x) - 1.)/(len(x)))

class Data_utility(object):
    # train and valid is the ratio of training set and validation set. test = 1 - train - valid
    def __init__(self, args):
        self.cuda = args.cuda;
        #self.model_D = args.model_D;
        self.P = args.window;
        self.h = args.horizon
        
        self.pre_win = args.pre_win; 

        self.data_org = loadmat(args.data_org_path)['expression']
        #self.graph_org = loadmat(args.graph_org_path)['A']
        self.data_change = loadmat(args.data_change_path)['expression']
        #self.graph_change = sio.loadmat(args.graph_change_path)['A']
        #self.rawdat = loadmat(args.data)['expression']
        
        #self.data_change = self.data_change[0:int(0.8*len(self.data_org))]

        self.n_org, self.m = self.data_org.shape;       
        self.n_change = self.data_change.shape[0];

        self._split(args);
        
        #self._split_pre(args);


    def _split(self, args):        
        train_orgset = range(self.P+self.h-1, int(args.train * self.n_org));
        valid_orgset = range(int(args.train * self.n_org), int((args.train+args.valid) * self.n_org));
        test_orgset = range(int((args.train+args.valid) * self.n_org), self.n_org);
        self.train_org = self._batchify(train_orgset, self.h, self.data_org, 0);
        self.valid_org = self._batchify(valid_orgset, self.h, self.data_org, 0);
        self.test_org = self._batchify(test_orgset, self.h, self.data_org, 0);
        
        train_changeset = range(self.P+self.h-1, int(args.train * self.n_change));
        valid_changeset = range(int(args.train * self.n_change), int((args.train+args.valid) * self.n_change));
        test_changeset = range(int((args.train+args.valid) * self.n_change), self.n_change);
        self.train_change = self._batchify(train_changeset, self.h, self.data_change, 1);
        self.valid_change = self._batchify(valid_changeset, self.h, self.data_change, 1);
        self.test_change = self._batchify(test_changeset, self.h, self.data_change, 1);
               
        if (args.train==args.valid):
            self.valid_org = self.test_org
            self.valid_change = self.test_change
        
#        #self.train = self.train_org
#        #self.valid = self.valid_org
#        #self.test = self.test_org
#        trainX = torch.cat((self.train_org[0], self.train_change[0]), 0)
#        validX = torch.cat((self.valid_org[0], self.valid_change[0]), 0)
#        testX = torch.cat((self.test_org[0], self.test_change[0]), 0)
#        
#        trainY = torch.cat((self.train_org[1], self.train_change[1]), 0)
#        validY = torch.cat((self.valid_org[1], self.valid_change[1]), 0)
#        testY = torch.cat((self.test_org[1], self.test_change[1]), 0)
#            
#        self.train = [trainX, trainY]
#        self.valid = [validX, validY]
#        self.test = [testX, testY]

#    def _batchify_org(self, idx_set, horizon):
#        n = len(idx_set);
#        X = torch.zeros((n, self.P, self.m));
#        Y = torch.zeros((n, 1));                
#        for i in range(n-self.P+1):
#            end_X = idx_set[i];
#            start_X = end_X - self.P;     
#            np_x = self.data_org[start_X:end_X, :]
#            X[i,:self.P,:] = torch.from_numpy(np_x);           
#        return [X, Y];
#
#    def _batchify_change(self, idx_set, horizon):
#        n = len(idx_set);
#        X = torch.zeros((n, self.P, self.m));
#        Y = torch.ones((n, 1));                
#        for i in range(n-self.P+1):
#            end_X = idx_set[i];
#            start_X = end_X - self.P;     
#            np_x = self.data_change[start_X:end_X, :]
#            X[i,:self.P,:] = torch.from_numpy(np_x);            
#        return [X, Y];
    
    def _batchify(self, idx_set, horizon, data, label):
        n = len(idx_set);
        X = torch.zeros((n, self.P, self.m));
        if label == 0:
            Label = torch.zeros((n, 1)); 
        else:
            Label = torch.ones((n, 1));  
            
        if self.pre_win == 1:
            Y_pre = torch.zeros((n, self.m));
            Y_long = torch.zeros((n, self.P, self.m));
        else:        
            Y_pre = torch.zeros((n, self.pre_win, self.m)); 
            Y_long = torch.zeros((n, self.P, self.pre_win, self.m));
              
        for i in range(n-self.pre_win+1):
            end_X = idx_set[i];
            start_X = end_X - self.P;     
            np_x = data[start_X:end_X, :]
            X[i,:self.P,:] = torch.from_numpy(np_x);  
#            if self.pre_win ==1:
#                norm_y = self.data_org[idx_set[i]-1, :]
#                Y_pre[i,:] = torch.from_numpy(norm_y);
#            else:    
#                norm_y = self.data_org[idx_set[i]-self.pre_win:idx_set[i], :]
#                Y_pre[i,:,:] = torch.from_numpy(norm_y); 
            if self.pre_win ==1:
                norm_y = data[idx_set[i], :]
                Y_pre[i,:] = torch.from_numpy(norm_y);
                Y_long[i,:,:] = torch.from_numpy(data[(start_X+self.pre_win) : (end_X+self.pre_win), :])
                
            else:    
                norm_y = data[idx_set[i]:idx_set[i]+self.pre_win, :]
                Y_pre[i,:,:] = torch.from_numpy(norm_y);
                for pre_win_j in range(self.pre_win):
                    Y_long[i,:,pre_win_j,:] = torch.from_numpy(data[(start_X+pre_win_j+1) : (end_X+pre_win_j+1), :])
                
      
        return [X, Label, Y_pre, Y_long];
        #index = torch.randperm(len(X))
        #return [X[index], Label[index], Y_pre[index], Y_long[index]];
    
#


    def get_batches(self, this_data, batch_size, shuffle = False):
        
        inputs = this_data[0]; 
        labels = this_data[1];
        targets = this_data[2];
        

        length = len(inputs)
        if shuffle:
            index = torch.randperm(length)
        else:
            index = torch.LongTensor(range(length))
        start_idx = 0
        while (start_idx < length):
            end_idx = min(length, start_idx + batch_size)
            excerpt = index[start_idx:end_idx]
            X = inputs[excerpt]; 
            Label = labels[excerpt];
            Y = targets[excerpt];
            
            if (self.cuda):
                X = X.cuda();
                Label = Label.cuda();
                Y = Y.cuda();
                
            #pdb.set_trace()
            model_inputs = [Variable(X)];

            data = [model_inputs, Variable(Label), Variable(Y)]
            yield data;
#            return data
            start_idx += batch_size


