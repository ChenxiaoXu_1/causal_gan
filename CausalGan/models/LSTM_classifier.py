import torch
import torch.nn as nn
import pdb
import torch.nn.functional as F
from torch.nn.parameter import Parameter
import numpy as np
from sklearn.preprocessing import normalize
import matplotlib.pyplot as plt
from torch.autograd import Variable
## granger full rank
class Model(nn.Module):
    def __init__(self, args, data):
        super(Model, self).__init__()

        self.m = data.m
        self.lstm_hidden_size = args.lstm_hidden_size
        self.num_class = args.num_class


        self.H_init1 = torch.randn(1, 1, self.lstm_hidden_size)#nn.Parameter(torch.zeros(1, 1, self.lstm_hidden_size), requires_grad=True) 
        self.H_init2 = torch.randn(1, 1, self.lstm_hidden_size)#nn.Parameter(torch.zeros(1, 1, self.lstm_hidden_size), requires_grad=True) 
        
        self.H_init = (self.H_init1, self.H_init2)
        
        self.linears = [ nn.LSTM(self.m, self.lstm_hidden_size)]; #w->hid
        self.linears.append( nn.utils.weight_norm(nn.Linear(self.lstm_hidden_size, self.num_class, bias = True))); 
        
        self.linears = nn.ModuleList(self.linears);
        self.dropout = nn.Dropout(args.dropout);



    
    def forward(self, inputs):
        
        x_input = inputs[0] #pxm 
        batch_size = x_input.size(0);
        
        y_all_tmp = torch.zeros((batch_size, self.lstm_hidden_size))
        hidden = self.H_init
        for t_idx in range(batch_size):
            inputs = x_input[t_idx]
            inputs = inputs[:,None,:] 
            out, hidden = self.linears[0](inputs, hidden)
            y_all_tmp[t_idx] = out[-1]
            
        final_y = self.linears[1](y_all_tmp)
        final_y = torch.sigmoid(final_y);

        return final_y      
 


    
